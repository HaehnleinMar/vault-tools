#!/usr/bin/env bash

RECIPIENTS=""

while read recipient; do
  RECIPIENTS+=" -r \"${recipient}\""
done <vault/.gpg-id

openssl rand -base64 32 | eval gpg -e $RECIPIENTS -o vault/vault_passphrase.gpg
